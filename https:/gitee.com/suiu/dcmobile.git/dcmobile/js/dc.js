mui.init({
	swipeBack: true //启用右滑关闭功能
});
mui('#scroll').scroll({
	indicators: true //是否显示滚动条
});
mui('#scroll1').scroll({
	indicators: true //是否显示滚动条
});
mui('#scroll2').scroll({
	indicators: true //是否显示滚动条
});
$(function() {
	$("#tjbk").on("tap", function() {
		mui.openWindow({
			id: "add_dc",
			url: "add_dc.html",
			createNew: true
		})
	});
	$("#scbk").on("tap", function() {
		var arr = $(".mui-checkbox input[type='checkbox']:checkbox:checked");
		if(arr.length <= 0) {
			mui.toast("未选择删除项");
			return false;
		}
		var idstr = "";
		arr.each(function() {
			var myid = $(this).attr("myid");
			idstr += myid + ",";
		});
		if(idstr != '') {
			idstr = idstr.substring(0, idstr.length - 1);
		}
		$.ajax({
			type: "DELETE",
			url: dataDomain + "/dc/info?ids=" + idstr,
			async: true,
			success: function(req) {
				if(req.status == 'ok') {
					mui.toast("删除成功");
					weibkfun();
					dashfun();
					yibkfun();
				} else {
					mui.toast("删除失败，请稍候再试");
				}
			}
		});
		return false;
	});
	var userPicker = new mui.PopPicker();
	$.get(dataDomain + "/common/auditusers", function(req) {
		userPicker.setData(req);
	});
	$("#tjsh").on("tap", function() {
		var arr = $(".mui-checkbox input[type='checkbox']:checkbox:checked");
		if(arr.length <= 0) {
			mui.toast("未选择提交项");
			return false;
		}

		var idstr = "";

		arr.each(function() {
			var myid = $(this).attr("myid");
			idstr += myid + ",";
		});
		userPicker.show(function(items) {
			var obj = items[0];
			$.ajax({
				type: "POST",
				url: dataDomain + "/dc/info/audit",
				data: {
					ids: idstr,
					auditers: obj.value
				},
				async: true,
				success: function(req) {
					if(req.status == 'ok') {
						mui.toast("提交审批成功");
						weibkfun();
						dashfun();
						yibkfun();
					} else {
						mui.toast("提交审批失败，请稍候再试");
					}
				}
			});
		});

		return false;
	})
	$("#bk").on("tap", function() {
		mui.openWindow({
			url: "dc.html",
			id: "dc",
			createNew: true
		});
		$(".mui-bar-tab a").removeClass("mui-active");
		$("#bk").addClass("mui-active");
		return false;
	});
	$("#bj").on("tap", function() {
		mui.openWindow({
			url: "index.html",
			id: "bj",
			createNew: true
		});
		$(".mui-bar-tab a").removeClass("mui-active");
		$("#bk").addClass("mui-active");
		return false;
	});
	$("#wd").on("tap", function() {
		mui.openWindow({
			url: "setting.html",
			id: "wd",
			createNew: true
		});
		$(".mui-bar-tab a").removeClass("mui-active");
		$("#bk").addClass("mui-active");
		return false;
	});

	function weibkfun() {
		$.get(dataDomain + "/dc/infos?state=0", function(req) {
			if(req.data.length > 0) {
				var htm = '';
				for(var i = 0; i < req.data.length; i++) {
					htm += '<li class="mui-table-view-cell mui-left">';
					htm += '<span style="padding-left:30px;" class="mui-table-view-cell mui-checkbox mui-left"><input style="left:10px;" myid="' + req.data[i].id + '" type="checkbox" /></span><span myid="' + req.data[i].id + '" class="view-detail1">' + req.data[i].realName;
					htm += '</span></li>';
				}
				$("#weibk").html(htm);
				$(".view-detail1").on("tap", function() {
					var myid = $(this).attr("myid");
					mui.openWindow({
						url: "add_dc.html",
						id: "edit_dc",
						createNew: true,
						extras: {
							mainid: myid
						}
					})
				});
			}
		});
	}

	function dashfun() {
		$.get(dataDomain + "/dc/infos?state=1", function(req) {
			if(req.data.length > 0) {
				var htm = '';
				for(var i = 0; i < req.data.length; i++) {
					htm += '<li class="mui-table-view-cell mui-left">';
					if(adminuser.roleNo == '1002') {
						htm += '<span style="padding-left:30px;" class="mui-table-view-cell mui-checkbox mui-left"><input style="left:10px;" myid="' + req.data[i].id + '" type="checkbox" /></span>';
					}
					htm += '<span myid="' + req.data[i].id + '" class="view-detail2">' + req.data[i].realName;
					var auditTypeName = '';
					if(req.data[i].auditType == 0) {
						auditTypeName = '取消布控';
					} else {
						auditTypeName = '开启布控';
					}
					htm += '&nbsp;&nbsp;' + req.data[i].commitAuditDate + '&nbsp;&nbsp;' + auditTypeName + "</span>";
					htm += '</li>';
				}
				$("#daish").html(htm);
				$(".view-detail2").on("tap", function() {
					var myid = $(this).attr("myid");
					mui.openWindow({
						url: "add_dc.html",
						id: "edit_dc",
						createNew: true,
						extras: {
							mainid: myid
						}
					})
				});
			}
		});
	}

	function yibkfun() {
		$.get(dataDomain + "/dc/infos?state=2", function(req) {
			if(req.data.length > 0) {
				var htm = '';
				for(var i = 0; i < req.data.length; i++) {
					htm += '<li class="mui-table-view-cell mui-left">';
					htm += '<span style="padding-left:30px;" class="mui-table-view-cell mui-checkbox mui-left"><input style="left:10px;" myid="' + req.data[i].id + '"  name="checkbox" type="checkbox" /></span><span myid="' + req.data[i].id + '" class="view-detail3">' + req.data[i].realName;
					htm += '</span></li>';
				}
				$("#yibk").html(htm);
				$(".view-detail3").on("tap", function() {
					var myid = $(this).attr("myid");
					mui.openWindow({
						url: "add_dc.html",
						id: "edit_dc",
						createNew: true,
						extras: {
							mainid: myid
						}
					})
				});
			}
		});
	}
	weibkfun();
	dashfun();
	yibkfun();
	var adminuser = JSON.parse(localStorage.adminuser);
	//普通民警
	if(adminuser.roleNo == '1001') {
		$("#tjsh").parent().show();
	} else if(adminuser.roleNo == '1002') { //中层领导
		$("#tjbk1").parent().show();
	} else if(adminuser.roleNo == '1003') { //高层领导
		$("#tjbk1").parent().show();
		$("#daishenhe").hide();
	}
	$("#tjbk1").on("tap", function() {
		var arr = $(".mui-checkbox input[type='checkbox']:checkbox:checked");
		if(arr.length <= 0) {
			mui.toast("未选择提交项");
			return false;
		}
		var idstr = "";
		arr.each(function() {
			var myid = $(this).attr("myid");
			idstr += myid + ",";
		});
		$.post(dataDomain + "/dc/info/commit", {
			ids: idstr
		}, function(req) {
			if(req.status == 'ok') {
				mui.toast("提交布控成功");
				weibkfun();
				dashfun();
				yibkfun();
			} else {
				mui.toast("提交失败，请稍候再试");
			}
		});
	});
	$("#segmentedControl a").on("tap", function() {
		var txt = $.trim($(this).text());
		if(txt == '未布控') {
			$("#rightbtn").show();
			$("#rightbtn").attr("href", "#topPopover");
			$(".mui-popover").css("height", "150px");
		} else if(txt == '待审核') {
			if(adminuser.roleNo == '1002') {
				$("#rightbtn").attr("href", "#topPopoverShbk");
				$(".mui-popover").css("height", "100px");
			} else {
				$("#rightbtn").hide();
			}
		} else if(txt == '已布控') {
			$("#rightbtn").show();
			$("#rightbtn").attr("href", "#topPopoverQxbk");
			$(".mui-popover").css("height", "50px");
		}
	});
	$("#quxiaobukong").on("tap", function() {
		var arr = $(".mui-checkbox input[type='checkbox']:checkbox:checked");
		if(arr.length <= 0) {
			mui.toast("未选择提交项");
			return false;
		}

		var idstr = "";

		arr.each(function() {
			var myid = $(this).attr("myid");
			idstr += myid + ",";
		});
		//如果是民警
		if(adminuser.roleNo == '1001') {
			userPicker.show(function(items) {
				var obj = items[0];
				$.post(dataDomain + "/dc/info/applyclose", {
					auditers: obj.value,
					ids: idstr,
					remark: ""
				}, function(req) {
					if(req.status == 'ok') {
						mui.toast("提交成功");
						weibkfun();
						dashfun();
						yibkfun();
					} else {
						mui.toast("提交失败，请稍候再试");
					}
				});
			});
		} else {
			$.post(dataDomain + "/dc/info/close", {
				ids: idstr
			}, function(req) {
				console.log(req.status)
				if(req.status == 'ok') {
					mui.toast("提交成功");
					weibkfun();
					dashfun();
					yibkfun();
				} else {
					mui.toast("提交失败，请稍候再试");
				}
			});
		}
	});
	$("#tongguo").on("tap", function() {
		var arr = $(".mui-checkbox input[type='checkbox']:checkbox:checked");
		if(arr.length <= 0) {
			mui.toast("未选择提交项");
			return false;
		}

		var idstr = "";

		arr.each(function() {
			var myid = $(this).attr("myid");
			idstr += myid + ",";
		});
		$.post(dataDomain + "/dc/info/pass", {
			ids: idstr
		}, function(req) {
			if(req.status == 'ok') {
				mui.toast("审批已成功提交");
				weibkfun();
				dashfun();
				yibkfun();
			} else {
				mui.toast("提交失败，请稍候再试");
			}
		});
	});
	$("#jujue").on("tap", function() {
		var arr = $(".mui-checkbox input[type='checkbox']:checkbox:checked");
		if(arr.length <= 0) {
			mui.toast("未选择提交项");
			return false;
		}

		var idstr = "";

		arr.each(function() {
			var myid = $(this).attr("myid");
			idstr += myid + ",";
		});
		$.post(dataDomain + "/dc/info/unpass", {
			ids: idstr
		}, function(req) {
			if(req.status == 'ok') {
				mui.toast("审批已成功提交");
				weibkfun();
				dashfun();
				yibkfun();
			} else {
				mui.toast("提交失败，请稍候再试");
			}
		});
	});
});