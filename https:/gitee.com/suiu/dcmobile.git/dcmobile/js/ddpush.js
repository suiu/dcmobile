function addDDPushPlugin()
{
	var _BARCODE = 'DDPush',
        B = window.plus.bridge;
    var DDPush = {
    		"start":function(_ip,_port,_userName){
    			return B.exec(_BARCODE, "start", [_ip, _port, _userName]);
    		},
    		"stop":function(){
			    return B.exec(_BARCODE, "stop", []);
    		}
    };
    window.plus.DDPush = DDPush;
}

if(window.plus){
    addDDPushPlugin();
}else{
    document.addEventListener('plusready',addDDPushPlugin,false);
}




